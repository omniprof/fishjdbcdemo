package com.kenfogel.fish_jdbc_demo.business;

import com.kenfogel.fish_jdbc_demo.beans.FishData;
import com.kenfogel.fish_jdbc_demo.persistence.FishDAO;
import com.kenfogel.fish_jdbc_demo.persistence.FishDAOImpl;
import java.sql.SQLException;
import java.util.List;

/**
 * Business class that uses the persistence layer to retrieve records
 *
 * @author Ken Fogel
 * @version 1.1
 *
 */
public class FishManager {

    private final FishDAO fishDAO;

    /**
     * Constructor
     */
    public FishManager() {
        super();
        fishDAO = new FishDAOImpl();
    }

    /**
     * This method retrieves all the records and returns them as a string so
     * that the string can be displayed in the UI
     *
     * @return String containing all the fish
     */
    public String retrieveFish() {

        StringBuilder sb = new StringBuilder();

        List<FishData> data;
        try {
            data = fishDAO.findAll();
            data.stream().forEach((fd) -> {
                sb.append(fd.toString()).append("\n");
            });
        } catch (SQLException e) {
            e.printStackTrace();
            sb.append("\nSQL Error ").append(e.getMessage());
        }
        return sb.toString();
    }
}
